\select@language {english}
\contentsline {section}{\numberline {1}Disclaimer}{2}{section.1}
\contentsline {section}{\numberline {2}Introduction}{2}{section.2}
\contentsline {section}{\numberline {3}Clues and solves chronologically}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}ARG}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The address}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}The morse code}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}The Endercode}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}The Minecraft Server}{8}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Onions}{13}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}The cheshire Cat?}{14}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Roll your dice on search}{17}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Play it Again, Sam}{18}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Everywhere at once}{19}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}The game within the game}{20}{subsection.3.11}
\contentsline {subsection}{\numberline {3.12}We are going to the moon}{21}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}ROTARY file}{22}{subsection.3.13}
\contentsline {subsection}{\numberline {3.14}I want to believe}{23}{subsection.3.14}
\contentsline {subsection}{\numberline {3.15}Such wow!}{25}{subsection.3.15}
\contentsline {subsection}{\numberline {3.16}...or is it?}{29}{subsection.3.16}
\contentsline {section}{\numberline {4}The very short version}{29}{section.4}
\contentsline {section}{\numberline {5}My personal synopsis}{31}{section.5}
